package Servlets;

import Classes.Posts;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.mongodb.*;
import com.mongodb.util.JSON;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet("/FBController")
public class FBController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public FBController() {
		super();
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException  {

		String FBPostsJson = ""; //request.getParameter("FBPage");
		String data[]=null; //Vetor utilizado para quebrar os dados que vem do JS
		String FBId = ""; // Para recebero Id da pagina do FB
		BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream(),"UTF-8"));

		
		if (br!=null){
			FBPostsJson = br.readLine();
			System.out.println(FBPostsJson);
			data = FBPostsJson.split("@-@-@");
			FBId = data[0];
			FBPostsJson = data[1];
			//	FBPostsJson = FBPostsJson.substring(0, FBPostsJson.length() - 2);
			System.out.println(FBPostsJson);
		}


		MongoClient mongoClient = new MongoClient( "18.228.12.57" , 27017 );
		//MongoClient mongoClient = new MongoClient( "localhost" , 27017 );
		DB db = mongoClient.getDB("convexa_social_media");
		DBCollection coll = db.getCollection("Unimed_Federacao");
		mongoClient.setWriteConcern(WriteConcern.JOURNALED);

		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		Posts posts = mapper.readValue(FBPostsJson, Posts.class);

		for(int x = 0; x < posts.data.size(); x++){
			String dataOriginal = posts.data.get(x).created_time;
			dataOriginal = dataOriginal.replace("T", " ").replace("+", ".");
			Date temp = null;
			try {
				temp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSS").parse(dataOriginal);
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			


			String jsonPost = new Gson().toJson(posts.data.get(x));
			System.out.println(jsonPost);
			DBObject dbObject = (DBObject) JSON.parse(jsonPost);
			dbObject.put("formated_date", temp);
			System.out.println(dbObject);
			//user.insert(dbObject);
			coll.insert(dbObject);

		}
		System.out.println("Finalizado!");

	}

}
