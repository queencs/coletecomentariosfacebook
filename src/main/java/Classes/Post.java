package Classes;

import java.util.ArrayList;

public class Post {
	public String id;
	public String message;
	public String link;
	public String type;
	public Comments comments;
	public Share shares;
	public int sharesCount;
	public int likesCount;
	public int reactionsCount;
	public int commentsCount;
	public FBLikes likes;
	public FBReactions reactions;
	public String pageid;
	public String created_time;
	public String created_year;
	public String created_moth;
	public String created_day;
	public String created_hour;
	public String created_min;
	public String created_sec;
}
